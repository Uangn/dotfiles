;;;;;;;;;; Package Management ;;;;;;;;;;

(require 'package) ; load the package manager
(setq package-check-signature nil) ; override signature errors
;; add package archives to package manager
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(package-initialize) ; exactly what it sounds like 
;; pull archvies and prevent warning messages only on very first startup
(unless package-archive-contents
  (progn
    (setq warning-minimum-level :emergency) 
    (package-refresh-contents)))
;; install use-package if it doesn't exist yet
(unless (package-installed-p 'use-package) 
  (package-install 'use-package))          
(require 'use-package) ; load use-package
;; Make use-package uses package.el, prevents having to use :ensure t on everything
(setq use-package-always-ensure t) 


;;;;;;;;;; Keybinds ;;;;;;;;;;

(use-package evil 
  :init
  (setq evil-want-keybinding nil) ; needed when using with evil collection
  (setq evil-want-C-u-scroll t) ; i wanna C-u scrool
  (setq evil-undo-system 'undo-fu)
  :config
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :config
  (setq evil-collection-mode-list '(magit term help dashboard dired ibuffer tetris))
  (evil-collection-init))

(use-package general
  :config
  ;; By default, to escape the mini-buffer, you need to hit ESC 3 times, this
  ;; bind changes that, so it only takes one.
  (general-define-key
    :keymaps 'minibuffer-local-map
    "<escape>" #'keyboard-escape-quit)

  ;; Let an active leader key for normal, visual, and emacs states
  (general-create-definer leader
    :states '(normal visual emacs)
    :keymaps 'override
    :prefix "SPC" ;; set leader
    :global-prefix "M-SPC") ;; access leader in insert mode

  (leader
    "s" '(:ignore t :wk "Search")
    "s f" '(find-file :wk "Search file"))

  (leader
    "p" '(:ignore t :wk "Pane")
    "p f" '(switch-to-buffer* :wk "Find a Pane, or create a new one")
    "p k" '(kill-this-buffer :wk "Kill the current Pane")
    "p r" '(revert-buffer :wk "Reload the current Pane"))

  (leader
    "c" '(:ignore t :wk "Comment")
    "c c" '(comment-region :wk "Comment selection")
    "c C" '(comment-line :wk "Comment line"))

  (leader
    "?" '(:ignore t :wk "Help")
    "? f" '(describe-function :wk "Help function")
    "? v" '(describe-variable :wk "Help variable")
    "? m" '(describe-mode :wk "Help mode")
    "? c" '(describe-char :wk "Help character")
    "? k" '(describe-key :wk "Help key/keybind"))

  (leader
    "h" '(:ignore t :wk "Harpoon")
    ;; You can use this hydra menu that have all the commands
    "h q" '(harpoon-quick-menu-hydra :wk "Harpoon Menu")

    "h m" '(harpoon-add-file :wk "Harpoon Mark")
    ;"h f" '(harpoon-toggle-file :wk "Harpoon toggle file")
    "h l" '(harpoon-toggle-quick-menu :wk "Harpoon Toggle Quick Menu")
    "h c" '(harpoon-clear :wk "Harpoon Clear List")

    "h f" '(harpoon-go-to-1 :wk "Harpoon Goto File 1")
    "h d" '(harpoon-go-to-2 :wk "Harpoon Goto File 2")
    "h s" '(harpoon-go-to-3 :wk "Harpoon Goto File 3")
    "h a" '(harpoon-go-to-4 :wk "Harpoon Goto File 4"))

  ;; tmux sometimes doesnt work on some systems
  (leader
    "y" '(clipetty-kill-ring-save :wk "Yank to System Clipboard")))

(use-package which-key
  :init
  (which-key-mode 1)
  :config
  (setq which-key-side-window-location 'bottom
  		which-key-sort-order #'which-key-key-order-alpha
  		which-key-sort-uppercase-first nil
  		which-key-add-column-padding 1
  		which-key-max-display-columns nil
  		which-key-min-display-lines 6
  		which-key-side-window-slot -10
  		which-key-side-window-max-height 0.25
  		which-key-idle-delay 0.8
  		which-key-max-description-length 25
  		which-key-allow-imprecise-window-fit t
  		which-key-separator " → " )
  )


;;;;;;;;;; Functionality ;;;;;;;;;;

;; Undo
(use-package undo-fu)

;; Clipboard
(use-package clipetty
  :ensure t)

;; Minibuffer
(use-package vertico
  :general
  ;; you probably want this, lets backspace delete and entire directory completion, instead of
  ;; one char at a time.
  (:keymaps 'vertico-map
    "<backspace>" #'vertico-directory-delete-char
    "DEL" #'vertico-directory-delete-char)
  :init
  (vertico-mode))

(use-package marginalia
  :init
  (marginalia-mode))

(use-package orderless
  :config
  (setq completion-styles '(orderless basic)
        completion-category-overrides '((file (styles basic partial-completion)))))

;; Completion
(use-package corfu
  :config
  (setq corfu-popupinfo-delay 0
        corfu-auto t
        corfu-cycle t
        corfu-preselect 'prompt
        corfu-auto-delay 0.2
        corfu-auto-prefix 2)
  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster)
  :init
  (corfu-popupinfo-mode)
  (global-corfu-mode)
  (corfu-history-mode))

(use-package cape
  :init
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-elisp-block)
  (add-to-list 'completion-at-point-functions #'cape-keyword))

;; File Navigation
(use-package harpoon)

;;;;;;;;;; Style ;;;;;;;;;;

(use-package catppuccin-theme
  :config
  (setq catppuccin-highlight-matches t)
  (setq catppuccin-flavor 'latte)
  (load-theme 'catppuccin t))

(use-package dashboard
  :config
  (dashboard-setup-startup-hook))

;; Better highlighting in dired buffers
(use-package diredfl
  :config
  (diredfl-global-mode))

;;;;;;;;;; Preferences ;;;;;;;;;;

;; set font size to 18 point
(set-face-attribute 'default nil :height 180)
;; disable menus
(menu-bar-mode -1)
;; disable toolbar
(tool-bar-mode -1)
;; disable scrollbar
(scroll-bar-mode -1)
;; automatically close pairs like (), [] and {}
;(electric-pair-mode 1)
;; highlight the current line
(global-hl-line-mode)
;; automatically indent
(electric-indent-mode t)
;; display line numbers
(global-display-line-numbers-mode 1)
;; truncate lines, nowrap
(setq-default truncate-lines t)
;; stop emacs from inserting impertive configs into init.el
;; by dumping them into a custom.el file that will never be loaded
(setq custom-file (concat user-emacs-directory "/custom.el") 
      make-backup-files nil ; stop creating backup ~ files
      auto-save-default nil ; stop creating autosave # files
      create-lockfiles nil  ; stop creating lock .# files
      blink-cursor-mode nil ; exactly what is sounds like
      use-short-answers t   ; lets you type y,n instead of yes,no when prompted
      use-dialog-box nil    ; disable gui menu pop-ups
      display-line-numbers-type 'relative ; enable relative line numbers
      password-cache-expiry nil) ; prevents tramp passwords from expiring
;; Automatically refresh dired buffer when a change on disk is made
(add-hook 'dired-mode-hook 'auto-revert-mode)
