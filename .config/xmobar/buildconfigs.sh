cat settings template-horizontal > m0-xmobarrc
cat settings template-no-workspaces > m1-xmobarrc
cat settings template-battery-horizontal > battery-m0-xmobarrc
cat settings template-battery-no-workspaces > battery-m1-xmobarrc

# perl -0777 -pi -e 's/\\\n.*\\.*%_XMONAD_TRAYPAD%/\\/' m0-xmobarrc
# perl -0777 -pi -e 's/\\\n.*\\.*%_XMONAD_TRAYPAD%/\\/' battery-m0-xmobarrc

perl -0777 -pi -e 's/\\\n.*\\.*%_XMONAD_TRAYPAD%/\\/' m1-xmobarrc
perl -0777 -pi -e 's/\\\n.*\\.*%_XMONAD_TRAYPAD%/\\/' battery-m1-xmobarrc
