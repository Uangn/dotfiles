#!/bin/bash
xs=$(pactl list short sources | rg input.*RUNNING)

if [ -z "$xs" ]; then
    xs=$(pactl list short sources | rg input.*SUSPENDED)
fi

if [ -z "$xs" ]; then
    xs=$(pactl list short sources | rg input.*IDLE)
fi

echo "$xs" | awk -F'\t' '{system("pactl set-source-mute " $1 " toggle")}'
