#!/bin/bash
xs=$(pactl list short sources | rg input.*RUNNING)

if [ -z "$xs" ]; then
    xs=$(pactl list short sources | rg input.*SUSPENDED)
fi

if [ -z "$xs" ]; then
    xs=$(pactl list short sources | rg input.*IDLE)
fi

echo "$xs" | head -1 | awk -F'\t' '{system("pactl get-source-mute " $1)}' | awk '{if ($2 == "no") print ""; else print ""}'
