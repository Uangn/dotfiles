#!/bin/bash

SLEEP_TIME=1

while [[ "$#" -gt 0 ]]; do
    case "$1" in
        -on) ON_COMMAND="$2"; shift 2 ;;
        -off) OFF_COMMAND="$2"; shift 2 ;;
        -t) SLEEP_TIME="$2"; shift 2;;
        *) echo "Unknown option: $1"; exit 1 ;;
    esac
done

while true; do
    if [[ "$(pactl list sinks | grep 'State: RUNNING')" ]]; then
        eval "$ON_COMMAND"
    else
        eval "$OFF_COMMAND"
    fi

    sleep $SLEEP_TIME
done
