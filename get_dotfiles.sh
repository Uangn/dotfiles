#!/bin/sh

DOTS=$HOME/Documents/dotfiles

copything() {
    dir=$1
    shift
    if [[ ! -z "$dir" ]] then
        mkdir -p "$dir";
    fi
    for i in "$@"; do
        echo "From:" "$HOME/$i" "--- To:" "$DOTS/$dir"
        cp -r "$HOME/$i" "$DOTS/$dir"
    done
}

mkdir -p "$DOTS"

config=(
    ".config/alacritty"
    ".config/btop"
    ".config/dunst"
    ".config/i3"
    ".config/kitty"
    ".config/mpv"
    ".config/pcmanfm"
    ".config/picom"
    ".config/ranger"
    ".config/rofi"
    ".config/sddm-themes"
    ".config/starship.toml"
    ".config/xmobar"
)
copything '.config' "${config[@]}"

mkdir -p "$DOTS/.config/emacs"
cp "$HOME/.config/emacs/init.el" "$DOTS/.config/emacs/"
mkdir -p "$DOTS/.config/tmux"
cp "$HOME/.config/tmux/tmux.conf" "$DOTS/.config/tmux/"
cp -r "$HOME/.config/tmux/themes" "$DOTS/.config/tmux/"
mkdir -p "$DOTS/.config/discord"
cp "$HOME/.config/discord/settings.json" "$DOTS/.config/discord/"


localbin=(
    ".local/bin/scripts"
)
copything '.local/bin' "${localbin[@]}"


localshare=(
    ".local/share/rofi"
)
copything '.local/share' "${localshare[@]}"

home=(
    ".Xmodmap"
    ".bashrc.d"
    ".ghci"
    ".gtkrc-2.0"
    ".haskeline"
    ".inputrc"
    ".stalonetrayrc"
    ".xprofile"
)
copything '' "${home[@]}"


git submodule foreach git pull origin main
